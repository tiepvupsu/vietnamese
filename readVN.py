#!/usr/bin/python

# -*- coding: utf8 -*-
import codecs
import re 
import operator
import gen_dict 
from Unicode_VN import * 
from VN_utils import *
# code_dict = {
#     'xe0'   :'af', 
#     'xe1'   :'as', 
#     'u1ea3' :'ar', 
#     'xe3'   :'ax', 
#     'u1ea1' :'aj', 
#     'u0103' :'aw', 
#     'u1eb1' :'awf', 
#     'u1eaf' :'aws', 
#     'u1eb3' :'awr', 
#     'u1eb5' :'awx', 
#     'u1eb7' :'awj', 
#     'xe2'   :'aa', 
#     'u1ea7' :'aaf', 
#     'u1ea5' :'aas', 
#     'u1ea9' :'aar', 
#     'u1eab' :'aax', 
#     'u1ead' :'aaj', 
#     'u0111' :'dd', 
#     'xe8'   :'ef', 
#     'xe9'   :'es', 
#     'u1ebb' :'er', 
#     'u1ebd' :'ex', 
#     'u1eb9' :'ej', 
#     'xea'   :'ee', 
#     'u1ec1' :'eef', 
#     'u1ebf' :'ees', 
#     'u1ec3' :'eer', 
#     'u1ec5' :'eex', 
#     'u1ec7' :'eej', 
#     'xec'   :'if', 
#     'xed'   :'is', 
#     'u1ec9' :'ir', 
#     'u0129' :'ix', 
#     'u1ecb' :'ij', 
#     'xf2'   :'of', 
#     'xf3'   :'os', 
#     'u1ecf' :'or', 
#     'xf5'   :'ox', 
#     'u1ecd' :'oj', 
#     'xf4'   :'oo', 
#     'u1ed3' :'oof', 
#     'u1ed1' :'oos', 
#     'u1ed5' :'oor', 
#     'u1ed7' :'oox', 
#     'u1ed9' :'ooj', 
#     'u01a1' :'ow', 
#     'u1edd' :'owf', 
#     'u1edb' :'ows', 
#     'u1edf' :'owr', 
#     'u1ee1' :'owx', 
#     'u1ee3' :'owj', 
#     'xf9'   :'uf', 
#     'xfa'   :'us', 
#     'u1ee7' :'ur', 
#     'u0169' :'ux', 
#     'u1ee5' :'uj', 
#     'u01b0' :'uw', 
#     'u1eeb' :'uwf', 
#     'u1ee9' :'uws', 
#     'u1eed' :'uwr', 
#     'u1eef' :'uwx', 
#     'u1ef1' :'uwj', 
#     'u1ef3' :'yf', 
#     'xfd'   :'ys', 
#     'u1ef7' :'yr', 
#     'u1ef9' :'yx', 
#     'u1ef5' :'yj', 
#     'u0110' :'DD'
# }
# CODE = 1 # 0 for Telex, 1 for VNI 
# def convert2ascii(a, CODE = 1):
#     i = 2
#     res = ''
#     while i < len(a)-1:
#         if a[i] == '\\':
#             i += 1
#             if i < len(a)-1 and a[i] == '\\':
#                 # print '\\',
#                 res += '\\'
#                 i += 1 
#             elif i < len(a)-1 and a[i] == 'x':
#                 if a[i:i+3] in code_dict:
#                     res += code_dict[a[i:i+3]][CODE]
#                 i += 3 
#             elif i < len(a) - 1 and a[i] == 'u':
#                 if a[i:i+5] in code_dict:
#                     res += code_dict[a[i:i+5]][CODE]
#                 i += 5
#         else:
#             res += a[i]
#             i+= 1 
#         # print res 
#     return res 

# Chu = {}
# TuGhep  = {}

# def test():
#     Chu = {}
    
#     # f = codecs.open('../vn.txt', encoding='utf-8')
#     # f = codecs.open('VNTQcorpus-small.txt', encoding='utf-8')
#     # f = codecs.open('VNTQcorpus-big.txt', encoding='utf-8')
#     f = codecs.open('TruyenKieu.txt', encoding='utf-8')
#     # f = codecs.open('VNESEcorpus.txt', encoding='utf-8')
#     i = -1
#     for line in f:

#         i += 1 
#         # if i < 100:
#         #     continue
#         # if i >110:
#         #     break
#         # a= repr(line)
#         # print a 
#         # print repr(line)
#     # print a, len(a)
#         # print a 
#         str1 = repr(line)
#         str1 = str1.replace('\\r', '')
#         str1 = str1.replace('\\n', '')
#         a =  convert2ascii(str1) # remove \n at the end 
#         # i = 2 
#         res = a
#         # print res 
#         res2 = re.split(r'\W',res)
#         new = True 
#         for w in res2: 
#             w = w.lower()
#             if len(w) == 0 or not('a'<=w[0] <= 'z'):
#                 new = True  
#             # if len(w) > 0 and 'a' <= w[0] <= 'z':
#                 # print w.lower()
#                 # if w in Chu:
#                 #     Chu[w] += 1
#                 # else:
#                 #     Chu[w] = 1 
#             # tu ghep
#             else:
#                 if new:
#                     last = w 
#                     new = False 
#                 else: 
#                     word = last + ' ' + w  
#                     if word in TuGhep:
#                         TuGhep[word] += 1
#                     else:
#                         TuGhep[word] = 1
#                     last = w 
#     Chu = TuGhep
#     Chu = sorted(Chu.items(), key=lambda x: x[1], reverse = True)
#     i = 0
#     for chu in Chu:        
#         if chu[1] > 10:
#             i += 1
#             print str(i) + '. ' + str(chu[1]), ':', chu[0]

#     # print Chu 

#         # print u'\u0111'
#     f.close()
# test()
# print  'Tiep'

(Chu, TuGhep) = demchu_tu('TruyenKieu.txt', 5000)
print_dict(Chu, 30)
print_dict(TuGhep, 30)
